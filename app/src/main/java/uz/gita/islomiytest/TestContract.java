package uz.gita.islomiytest;

import android.view.View;

import uz.gita.islomiytest.model.room.QuestionEntity;

public interface TestContract {

    interface Model {
        int getCurrent();

        QuestionEntity getNextQuestion();

        QuestionEntity getCurrentQuestion();

        void reset();

        int getScore();

        void scoreIncrement();

        int getChance();

        void chanceDecrement();

        int getMaxQuestionNumber();

    }

    interface Presenter {

        void setView();

        void getNextQuestion(String answer, String trueAnswer);

        void setScore();

        void reset();

        void setChance();

        void setMaxQuestionNumber();


    }

    interface TestView {
        void init();

        void setMaxQuestionNumber(int totalQuestions);

        void userSelected(View v);

        void setScore(int score);

        void setChance(int chance);

        void setTest(QuestionEntity test);

        void clickedNextButton(View v);

        void printTrueAnswer(String message);

        void printWrongAnswer(String message);

        void finishTests(int numberOfQuestions, int score);

        void saveHistory(int score);

        void clickMusicBtn(View view);

    }
}