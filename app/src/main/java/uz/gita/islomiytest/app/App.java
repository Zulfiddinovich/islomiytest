package uz.gita.islomiytest.app;

import android.app.Application;

import uz.gita.islomiytest.model.LocalStorage;
import uz.gita.islomiytest.model.ModelImpl;
import uz.gita.islomiytest.model.room.QuestionDataBase;


public class App extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        LocalStorage.init(this);
        QuestionDataBase.initDB(this);
        ModelImpl.init();
    }
}
