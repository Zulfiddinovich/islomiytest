package uz.gita.islomiytest.model;

import android.util.Log;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import uz.gita.islomiytest.TestContract;
import uz.gita.islomiytest.model.room.QuestionDao;
import uz.gita.islomiytest.model.room.QuestionDataBase;
import uz.gita.islomiytest.model.room.QuestionEntity;

public class ModelImpl implements TestContract.Model {

    private QuestionDataBase questionDB;
    private QuestionDao dao;
    public static ModelImpl myObject;

    private List<QuestionEntity> tests;
    public QuestionEntity test;
    private int current = 0;
    private int score = 0;
    private int chance = 3;

    private ModelImpl() {
        tests = new ArrayList<>();
        questionDB = QuestionDataBase.getDatabase();
        dao = questionDB.getDao();
        tests.addAll(dao.getAll());
    }

    public static void init() {
        myObject = new ModelImpl();
    }

    @Override
    public QuestionEntity getNextQuestion() {
        if (tests.size() != 0) {
            Collections.shuffle(tests);
            test = tests.get(++current);
        } else test = null;
        return test;
    }

    @Override
    public QuestionEntity getCurrentQuestion() {
        if (tests.size() != 0) {
            if (test != null) {
                return test;
            } else {
                Collections.shuffle(tests);
                test = tests.get(current);
                return test;
            }
        } else {
            test = null;
            return test;
        }
    }

    @Override
    public void reset() {
        Log.d("TAG", "reset: Reseted");
        test = null;
        current = 0;
        score = 0;
        chance = 3;
    }

    @Override
    public int getCurrent() {
        return current;
    }

    @Override
    public int getScore() {
        return score;
    }

    @Override
    public void scoreIncrement() {
        score++;
    }

    @Override
    public int getChance() {
        return chance;
    }

    @Override
    public void chanceDecrement() {
        chance--;
    }


    @Override
    public int getMaxQuestionNumber() {
        return 15;
    }

}

/*public class ModelImpl implements TestContract.Model {
    private List<Test> tests;
    private int numberOfQuestion = 0;
    private int score = 0;
    private Test test;

    public ModelImpl() {
        tests = new ArrayList<>();
        database();
    }

    private void database() {
        tests.add(new Test(R.drawable.a1, "A"));
        tests.add(new Test(R.drawable.a2, "a"));
        tests.add(new Test(R.drawable.b, "B"));
        tests.add(new Test(R.drawable.d, "D"));
        tests.add(new Test(R.drawable.q, "q"));
        tests.add(new Test(R.drawable.q1, "Q"));
        tests.add(new Test(R.drawable.m, "M"));
        tests.add(new Test(R.drawable.m1, "m"));
        tests.add(new Test(R.drawable.k, "K"));
        tests.add(new Test(R.drawable.k1, "t"));
        tests.add(new Test(R.drawable.z, "Z"));
        tests.add(new Test(R.drawable.x, "X"));
        tests.add(new Test(R.drawable.ch, "ch"));
        tests.add(new Test(R.drawable.ch1, "Ch"));
        tests.add(new Test(R.drawable.v, "v"));
        tests.add(new Test(R.drawable.q2, "Q"));
        tests.add(new Test(R.drawable.f, "F"));
        tests.add(new Test(R.drawable.j, "J"));
        tests.add(new Test(R.drawable.y, "Y"));
        tests.add(new Test(R.drawable.l, "L"));
        tests.add(new Test(R.drawable.u, "U"));
        tests.add(new Test(R.drawable.o6, "o\'"));
        tests.add(new Test(R.drawable.i, "I"));
        tests.add(new Test(R.drawable.o, "O"));
        tests.add(new Test(R.drawable.s, "S"));
        tests.add(new Test(R.drawable.sh, "Sh"));
        tests.add(new Test(R.drawable.e, "E"));
        Collections.shuffle(tests);
    }

    @Override
    public int getNumberOfQuestions() {
        return numberOfQuestion;
    }

    @Override
    public Test getQuestion() {
        if (tests.size() != 0 && numberOfQuestion != 10) {
            test = tests.remove(0);
            numberOfQuestion++;
        } else test = null;
        return test;
    }

    @Override
    public int getScore() {
        return score;
    }

    @Override
    public boolean isCurrentlyAnswer(String answer) {
        if (test.getAnswer().equals(answer)) {
            score++;
            return true;
        } else
            return false;
    }

}*/
