package uz.gita.islomiytest.model.room;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

@Dao
public interface QuestionDao {

    @Query("SELECT * FROM questions")
    List<QuestionEntity>  getAll();


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void add(QuestionEntity question);

}
