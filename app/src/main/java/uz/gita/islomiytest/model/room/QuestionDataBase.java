package uz.gita.islomiytest.model.room;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = { QuestionEntity.class }, version = 1 )
public abstract class QuestionDataBase extends RoomDatabase {

    public abstract QuestionDao getDao();

    private static QuestionDataBase database;

    public static void initDB(Context context){
        database = Room.databaseBuilder(context, QuestionDataBase.class, "quiz_islomiy.db")
                .createFromAsset("quiz_islomiy.db")
                .allowMainThreadQueries()
                .build();
    }

    public static QuestionDataBase getDatabase(){ return database;}

}
