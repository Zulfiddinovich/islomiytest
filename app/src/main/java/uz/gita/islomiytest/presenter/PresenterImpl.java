package uz.gita.islomiytest.presenter;

import uz.gita.islomiytest.TestContract;
import uz.gita.islomiytest.model.ModelImpl;
import uz.gita.islomiytest.model.room.QuestionEntity;

public class PresenterImpl implements TestContract.Presenter {

    private TestContract.TestView view;
    private TestContract.Model model;
    private boolean isRespondTrue;
    private QuestionEntity test;

    public PresenterImpl(TestContract.TestView view) {
//        this.model = new ModelImpl();
        model = ModelImpl.myObject;
        this.view = view;
    }


    @Override
    public void setView() {
        test = model.getCurrentQuestion();
        if (test != null && model.getCurrent() <= model.getMaxQuestionNumber() && model.getChance() >= 0) {
            view.setTest(test);
            setScore();
            setMaxQuestionNumber();
            setChance();
        } else {
            view.saveHistory(model.getScore());
            view.finishTests(model.getCurrent(), model.getScore());
        }
    }

    @Override
    public void getNextQuestion(String answer, String trueAnswer) {
        ModelImpl.myObject.test = null;
        isRespondTrue = answer.equals(trueAnswer);
        if (isRespondTrue) {
            model.scoreIncrement();
        } else {
            model.chanceDecrement();
        }
            printModeAnswer(isRespondTrue);
            model.getNextQuestion();
            setView();
    }

    private void printModeAnswer(boolean modeAnswer) {
        if (modeAnswer) {
            view.printTrueAnswer("Javob to'g'ri");
        } else {
            view.printWrongAnswer(test.getOptionA());
        }
    }

    @Override
    public void setScore() {
        view.setScore(model.getScore());
    }

    @Override
    public void reset() {
        model.reset();
    }

    @Override
    public void setChance() {
        view.setChance(model.getChance());
    }

    @Override
    public void setMaxQuestionNumber() {
        view.setMaxQuestionNumber(model.getCurrent());
    }

}
