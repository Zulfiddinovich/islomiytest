package uz.gita.islomiytest.view.screen;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;

import uz.gita.islomiytest.R;

public class FinishActivity extends AppCompatActivity {
    private TextView finishText;
    private Button buttonRestart;
    private Button buttonRecord;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_finish);
        init();

        buttonRestart.setOnClickListener(v -> {
            Intent intent = new Intent(FinishActivity.this, MainActivityViewImpl.class);
            startActivity(intent);
            finish();
        });
        buttonRecord.setOnClickListener(v -> {
            Intent intent = new Intent(FinishActivity.this, RecordActivity.class);
            startActivity(intent);
            finish();
        });
    }

    private void init() {
        finishText = (TextView) findViewById(R.id.id_finish_text);
        buttonRestart = (Button) findViewById(R.id.id_restart_test);
        buttonRecord = (Button) findViewById(R.id.id_record_finish);

        Intent intent = getIntent();
        finishText.setText(intent.getStringExtra("finishText"));
    }
}
