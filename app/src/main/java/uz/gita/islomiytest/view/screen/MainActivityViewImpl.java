package uz.gita.islomiytest.view.screen;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import uz.gita.islomiytest.R;
import uz.gita.islomiytest.TestContract;
import uz.gita.islomiytest.model.LocalStorage;
import uz.gita.islomiytest.model.room.QuestionEntity;
import uz.gita.islomiytest.presenter.PresenterImpl;

public class MainActivityViewImpl extends AppCompatActivity implements TestContract.TestView {
    private TestContract.Presenter presenter;
    private TextView maxQuestionNumber, score, questionTitle, chance;
    private RadioButton selectedButton;
    private RadioGroup radioGroup;
    private Button buttonNext;
    private String selectedAnswer, trueAnswer;
    private MediaPlayer mediaPlayer;
    private boolean isRunning;
    private String keyIsRunningMusic = "mode_music";
    private String keyRecord = "history";
    private ImageView musicModeOnn, musicModeOff;
    private int media_length = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();

        ImageButton backButton = findViewById(R.id.back_button);
        backButton.setOnClickListener((listener) -> {
            onBackPressed();
        });


    }


    @Override
    public void init() {
        presenter = new PresenterImpl(this);
        maxQuestionNumber = (TextView) findViewById(R.id.number_question);
        score = (TextView) findViewById(R.id.number_currect);
        questionTitle = (TextView) findViewById(R.id.question_title);
        chance = (TextView) findViewById(R.id.chance);
        radioGroup = (RadioGroup) findViewById(R.id.containerAnswers);
        buttonNext = (Button) findViewById(R.id.button_next);
        buttonNext.setClickable(false);
        buttonNext.getBackground().setAlpha(100);
        musicModeOnn = (ImageView) findViewById(R.id.music_mode_onn);
        musicModeOff = (ImageView) findViewById(R.id.music_mode_off);
        presenter.setView();
    }


    @SuppressLint("SetTextI18n")
    @Override
    public void setMaxQuestionNumber(int totalQuestions) {
        maxQuestionNumber.setText(totalQuestions + "");
    }

    @Override
    public void userSelected(View v) {
        RadioButton a = (RadioButton) radioGroup.getChildAt(0);
        a.setTextColor(Color.parseColor("#AAAAAA"));
        RadioButton b = (RadioButton) radioGroup.getChildAt(1);
        b.setTextColor(Color.parseColor("#AAAAAA"));
        RadioButton c = (RadioButton) radioGroup.getChildAt(2);
        c.setTextColor(Color.parseColor("#AAAAAA"));


        selectedButton = (RadioButton) v;
        selectedButton.setTextColor(Color.WHITE);
        selectedAnswer = String.valueOf(selectedButton.getText());
        buttonNext.setClickable(true);
        buttonNext.getBackground().setAlpha(255);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void setScore(int score) {
        this.score.setText(score + "");
    }

    @Override
    public void setChance(int chance) {
        this.chance.setText(String.valueOf(chance));
    }

    @Override
    public void setTest(QuestionEntity test) {

        buttonNext.setClickable(false);
        buttonNext.getBackground().setAlpha(100);
        radioGroup.clearCheck();
        questionTitle.setText(test.getQuestion());
        List<String> list = new ArrayList();
        trueAnswer = test.getOptionA();
        list.add(test.getOptionA());
        list.add(test.getOptionB());
        list.add(test.getOptionC());
        Collections.shuffle(list);
        for (int i = 0; i < radioGroup.getChildCount(); i++) {
            RadioButton radioButton = (RadioButton) radioGroup.getChildAt(i);
            radioButton.setText(list.get(i));
            radioButton.setBackgroundColor(0);
        }
    }

    @Override
    public void clickedNextButton(View v) {
        presenter.getNextQuestion(selectedAnswer, trueAnswer);
        selectedButton.setTextColor(Color.parseColor("#AAAAAA"));
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void printTrueAnswer(String message) {
        LayoutInflater inflater = getLayoutInflater();
        View view = inflater.inflate(R.layout.custom_toast, ((ViewGroup) findViewById(R.id.id_custom_toast)));
        TextView toastText = view.findViewById(R.id.id_custom_toast_text);
        toastText.setText(message);
        Toast toast = new Toast(this);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(view);
        toast.show();
    }



    @SuppressLint("ResourceAsColor")
    @Override
    public void printWrongAnswer(String message) {
        LayoutInflater inflater = getLayoutInflater();
        View view = inflater.inflate(R.layout.custom_toast_wrong, ((ViewGroup) findViewById(R.id.id_custom_toast_wrong)));
        TextView toastText = view.findViewById(R.id.id_custom_toast_text);
        
        toastText.setText(toastText.getText() + message);
        Toast toast = new Toast(this);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(view);
        toast.show();
    }


    @Override
    public void finishTests(int current, int score) {
        current--;
        String string = "Siz " + current + " tadan " + score + " ta savolga to'g'ri javob berdingiz!";
        Intent intent = new Intent(this, FinishActivity.class);
        intent.putExtra("finishText", string);
        /* Qayta eski questionga qaytmasligi uchun va FinishActivityda to`g`ri scoreni ko`rsatishi uchun shu yerda yozildi*/
        presenter.reset();
        startActivity(intent);
        finish();
    }

    @Override
    public void saveHistory(int score) {
        Set<String> records = LocalStorage.preferences.getStringSet(keyRecord, null);
        Set<String> history = new HashSet<String>();

        if (records != null) {
            history.addAll(records);
        }

        history.add(String.valueOf(score));
        LocalStorage.editor.remove(keyRecord).apply();
        LocalStorage.editor.putStringSet(keyRecord, history).apply();
    }

    private void saveMusicMode(boolean modeMusic) {
        LocalStorage.editor.putBoolean(keyIsRunningMusic, modeMusic).apply();
    }

    public void clickMusicBtn(View view) {

        if (mediaPlayer.isPlaying()) {
            mediaPlayer.pause();
            media_length = mediaPlayer.getCurrentPosition();
            musicModeOnn.setVisibility(View.INVISIBLE);
            musicModeOff.setVisibility(View.VISIBLE);
            saveMusicMode(false);
        } else {
            mediaPlayer.seekTo(media_length);
            mediaPlayer.start();
            musicModeOff.setVisibility(View.INVISIBLE);
            musicModeOnn.setVisibility(View.VISIBLE);
            saveMusicMode(true);
        }
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        /*  mediani to`xtagan joyini Bundlega saqlayabdi  */
        if (mediaPlayer.isPlaying()) {
            mediaPlayer.pause();
            media_length = mediaPlayer.getCurrentPosition();
        }
        outState.putInt("media_length", media_length);
//        saveQuestionObject(outState);
    }

/*    private void saveQuestionObject(Bundle outState) {
        outState.putStringArray("question", new String[]{question.getQuestion(), question.getOptionA(), question.getOptionB(), question.getOptionC(), score.toString(), maxQuestionNumber.toString(), chance.toString()});
    }*/


    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        /*  onRestoredan mediani to`xtagan joyini olib yana valueni saqlayabdi  */
        media_length = savedInstanceState.getInt("media_length");
//        ArrayList<String> restoredQuestion = (ArrayList<String>) Arrays.asList(savedInstanceState.getStringArray("question"));
    }

    @Override
    protected void onResume() {
        super.onResume();

            /* Preferencedan MediaPlayerni running tekshirib olib yana start qilovoryabdi
                     Resume (rotate, home button qilib qaytganda) */
        mediaPlayer = MediaPlayer.create(this, R.raw.melodiya);
        isRunning = LocalStorage.preferences.getBoolean(keyIsRunningMusic, false);
        if (isRunning) {
            mediaPlayer.seekTo(media_length);
            mediaPlayer.start();
            mediaPlayer.setLooping(true);
            musicModeOff.setVisibility(View.INVISIBLE);
        } else {
            mediaPlayer.setLooping(true);
            musicModeOnn.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    protected void onDestroy() {
        /* heapda Mediaplayerni objecti qolib ketmasligi uchun (memory leak ni oldini olishga)  */
        if (mediaPlayer != null)
            mediaPlayer.stop();
        mediaPlayer = null;
        super.onDestroy();
    }


    @Override
    public void onBackPressed() {
        /* AlertDialog ga Custom rangi berilgan TextView*/
        TextView textView = new TextView(this);
        textView.setTextColor(Color.BLACK);
        textView.setPadding(30, 10, 10, 30);
        textView.setTextSize(20f);
        textView.setText("Testdan chiqishni xoxlaysizmi?");

        /* Testdan chiqishga AlertDialog */
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setCustomTitle(textView).setCancelable(true);
        alert.setPositiveButton("Xa", (dialogInterface, i) -> {
            presenter.reset();
            super.onBackPressed();
        });
        alert.setNeutralButton("Yo`q", (dialogInterface, i) -> {
            dialogInterface.dismiss();
        });
        alert.show();
    }
}