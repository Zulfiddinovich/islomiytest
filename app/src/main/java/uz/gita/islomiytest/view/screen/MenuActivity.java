package uz.gita.islomiytest.view.screen;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import uz.gita.islomiytest.R;

public class MenuActivity extends AppCompatActivity {
    private Button buttonTest;
    private Button buttonRecord;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);


        init();
        buttonTest.setOnClickListener(v->{
            Intent intent=new Intent(MenuActivity.this, MainActivityViewImpl.class);
            startActivity(intent);
        });
        buttonRecord.setOnClickListener(v->{
            Intent intent=new Intent(MenuActivity.this, RecordActivity.class);
            startActivity(intent);
        });
    }

    private void init() {
        buttonRecord = (Button) findViewById(R.id.id_record);
        buttonTest = (Button) findViewById(R.id.id_test);
    }

    @Override
    public void onBackPressed() {

        /* AlertDialog ga Custom rangi berilgan TextView*/
        TextView textView = new TextView(this);
        textView.setTextColor(Color.BLACK);
        textView.setPadding(30, 10, 10, 30);
        textView.setTextSize(20f);
        textView.setText("Dasturdan chiqishni xoxlaysizmi?");

        /* Testdan chiqishga AlertDialog */
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setCustomTitle(textView).setCancelable(true);
        alert.setPositiveButton("Xa", (dialogInterface, i) -> {
            finish();
        });
        alert.setNeutralButton("Yo`q", (dialogInterface, i) -> {
            dialogInterface.dismiss();
        });
        alert.show();
    }
}
