package uz.gita.islomiytest.view.screen;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.TreeSet;
import uz.gita.islomiytest.R;
import uz.gita.islomiytest.model.LocalStorage;

public class RecordActivity extends AppCompatActivity {
    private TreeSet<Integer> listRecords;
    private String keyRecord = "history";
    private HashSet<String> set;
    private LinearLayout containerLayout;
    private Button buttonTest;
    private Button buttonMenu;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_record);
        init();
        buttonTest.setOnClickListener(v -> {
            Intent intent = new Intent(RecordActivity.this, MainActivityViewImpl.class);
            startActivity(intent);
            finish();
        });
        buttonMenu.setOnClickListener(v -> {
            Intent intent = new Intent(RecordActivity.this, MenuActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();
        });
    }

    private void init() {
        listRecords = new TreeSet<>();
        set = (HashSet<String>) (LocalStorage.preferences.getStringSet(keyRecord, null));
        containerLayout = (LinearLayout) findViewById(R.id.id_container_players);
        invisibleRecords();
        buttonMenu = (Button) findViewById(R.id.id_menu);
        buttonTest = (Button) findViewById(R.id.id_back_test);
        if (set != null) {
            List<String> list = new ArrayList<>();
            list.addAll(set);
            while (!list.isEmpty()) {
                listRecords.add(Integer.valueOf(list.remove(0)));
            }
            int k = 0;
            if (listRecords.size() <= 5) {
                while (!listRecords.isEmpty()) {
                    LinearLayout visibleLinerLayout = (LinearLayout) containerLayout.getChildAt(k++);
                    visibleLinerLayout.setVisibility(View.VISIBLE);
                    String str = "Ball: " + String.valueOf(listRecords.pollLast());
                    ((TextView) visibleLinerLayout.getChildAt(1)).setText(str);
                }
            }
            else {
                TreeSet<Integer> set = new TreeSet<>();
                for (int i = 0; i < 5; i++) {
                    set.add(listRecords.pollLast());
                }
                while (!set.isEmpty()) {
                    LinearLayout visibleLinerLayout = (LinearLayout) containerLayout.getChildAt(k++);
                    visibleLinerLayout.setVisibility(View.VISIBLE);
                    String str = "Ball: " + String.valueOf(set.pollLast());
                    ((TextView) visibleLinerLayout.getChildAt(1)).setText(str);
                }
            }
        }
    }

    private void invisibleRecords() {
        for (int i = 0; i < containerLayout.getChildCount(); i++) {
            ((LinearLayout) containerLayout.getChildAt(i)).setVisibility(View.INVISIBLE);
        }
    }
}
